package ru.t1.dzelenin.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
